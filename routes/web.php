<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   // return view('welcome');
	 $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();
 
 // prefered option
 $website   = app(\Hyn\Tenancy\Environment::class)->tenant();

 // alternative (outdated) 
 $website   = app(\Hyn\Tenancy\Environment::class)->website();
 
 $websiteId = $website->id;
 
 // Get current Hostnames
 $hostname  = app(\Hyn\Tenancy\Environment::class)->hostname();

 
 // Get FQDN (Fully-Qualified Domain Name) by current hostname
 $fqdn      = $hostname->fqdn;
 dd($fqdn);

});
